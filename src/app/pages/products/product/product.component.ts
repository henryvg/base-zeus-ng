import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Product } from '../interfaces/products.interface';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent implements OnInit {

  @Input() product!: Product;
  @Output() handleAddToCart = new EventEmitter<Product>();

  constructor() { }

  ngOnInit(): void {
    console.log('')
  }

  onClick(): void {
    this.handleAddToCart.emit(this.product);
  }

}
