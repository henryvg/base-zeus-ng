import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators'

import { ShoppingCartService } from 'src/app/shared/services/shopping-cart.service';

import { Product } from '../../products/interfaces/products.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  columns: string[] = ['userId', 'id', 'title', 'body'];
  products: Product[] = [];
  total$ = this.shoppingCartService.totalAction$;
  cart$ = this.shoppingCartService.cartAction$;

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit(): void {
    this.cart$.subscribe((products) => this.products = (products));
  }

}
