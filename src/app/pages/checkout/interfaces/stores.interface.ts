export interface Store {
  id: number;
  name: string;
  phone: string;
  website: string;
}
