import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';

import { Store } from './interfaces/stores.interface';

import { CheckoutService } from './services/checkout.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  stores: Store[] = [];
  model = {
    name: '',
    store: '',
    shippingAddress: '',
    city: ''
  };

  constructor(private checkoutService: CheckoutService) { }

  ngOnInit(): void {
    this.checkoutService
      .getStores()
      .pipe(
        tap((stores: Store[]) => this.stores = stores)
      )
      .subscribe()
  }

  handleBuyOption(type: string): void {
    console.log(type)
  }

}
