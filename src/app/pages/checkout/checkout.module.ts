import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from '../../material.module';

import { CheckoutRoutingModule } from './checkout-routing.module';
import { CheckoutComponent } from './checkout.component';

import { ListComponent } from './list/list.component';


@NgModule({
  declarations: [
    CheckoutComponent,
    ListComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    MaterialModule,
    CheckoutRoutingModule
  ]
})
export class CheckoutModule { }
