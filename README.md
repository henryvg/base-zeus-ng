# TmZeusNg

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.13.

## Coding

(1) Generar un componente:
```
ng g c shared/components/header -m=app --skip-tests
```
(2) Generar una página con submódulos:
```
ng g m pages/products -m=app --route products
```
(3) Generar un servicio:
```
ng g s pages/products/services/products --skip-tests
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.